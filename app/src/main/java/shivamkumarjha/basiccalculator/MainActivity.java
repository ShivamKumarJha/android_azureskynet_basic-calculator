package shivamkumarjha.basiccalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button b1,b2,b3,b4;
    TextView result;
    EditText num1, num2;

    void init()
    {
        b1 = (Button) findViewById(R.id.add);
        b2 = (Button) findViewById(R.id.sub);
        b3 = (Button) findViewById(R.id.mul);
        b4 = (Button) findViewById(R.id.div);
        result = (TextView) findViewById(R.id.res);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        onClick();

    }

    public void onClick()
    {

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(1);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(2);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(3);
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(4);
            }
        });
    }

    public void isedittextempty(Integer n)
    {
        init();
        String number1 = num1.getText().toString();
        String number2 = num2.getText().toString();
        if (TextUtils.isEmpty(number1))
        {
            num1.setError("Enter number");
        }
        else if (TextUtils.isEmpty(number2))
        {
            num2.setError("Enter number");
        }
        else {
            double a, b;
            a = Double.parseDouble(number1);
            b = Double.parseDouble(number2);
            if (n == 1) {
                String r = Double.toString(a + b);
                result.setText(r);
            } else if (n == 2) {
                String r = Double.toString(a - b);
                result.setText(r);
            } else if (n == 3) {
                String r = Double.toString(a * b);
                result.setText(r);
            } else if (n == 4) {
                String r = Double.toString(a / b);
                result.setText(r);
            }
        }
    }
}
